<?php


namespace freezemage\datamanagement\control;


use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Fields\StringField;
use Bitrix\Main\ORM\Query\Join;


class BookTable extends DataManager {
    public static function getTableName() {
        return 'fm_book';
    }

    public static function getMap() {
        return array(
            (new IntegerField('ID'))
                ->configureAutocomplete(true)
                ->configurePrimary(true),
            (new StringField('NAME'))
                ->configureSize(255),
            (new IntegerField('AUTHOR_ID'))
                ->configureRequired(true),
            (new Reference(
                'AUTHOR',
                AuthorTable::getEntity(),
                Join::on('AUTHOR_ID', 'AUTHOR.ID')
            ))
        );
    }
}