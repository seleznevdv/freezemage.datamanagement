<?php


namespace freezemage\datamanagement\control;


use Bitrix\Main\DB\Connection;
use freezemage\datamanagement\entity\Author;


class AuthorRepository {
    private const ENTITY_PERSISTENCE_ID = 'fm_author';

    private $connection;
    private $queryFactory;

    public function __construct(Connection $connection, QueryFactory $queryFactory) {
        $this->connection = $connection;
        $this->queryFactory = $queryFactory;
    }

    public function findById(int $authorId): ?Author {
        $query = $this->queryFactory->createQuery(AuthorRepository::ENTITY_PERSISTENCE_ID);
        $result = $query->setSelect(array('*'))->where('ID', '=', $authorId)->exec();

        $author = $result->fetch();
        return ($author !== false) ? Author::fromArray($author) : null;
    }

    public function findByBookId(int $bookId): ?Author {
        $query = $this->queryFactory->createQuery(AuthorRepository::ENTITY_PERSISTENCE_ID);
        $result = $query->setSelect(array('*'))->where('BOOK.ID', '=', $bookId)->exec();

        $author = $result->fetch();
        return ($author !== false) ? Author::fromArray($author) : null;
    }

    public function add(Author $author): int {
        return $this->connection->add(
            AuthorRepository::ENTITY_PERSISTENCE_ID,
            $author->toArray()
        );
    }
}