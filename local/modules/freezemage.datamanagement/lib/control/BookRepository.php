<?php


namespace freezemage\datamanagement\control;


use Bitrix\Main\DB\Connection;
use Bitrix\Main\ORM\Query\Query;
use freezemage\datamanagement\entity\Book;


class BookRepository {
    private const ENTITY_PERSISTENCE_ID = 'fm_book';

    private $connection;
    private $queryFactory;

    public function __construct(Connection $connection, QueryFactory $queryFactory) {
        $this->connection = $connection;
        $this->queryFactory = $queryFactory;
    }

    public function findById(int $bookId): ?Book {
        $query = $this->queryFactory->createQuery(BookRepository::ENTITY_PERSISTENCE_ID);
        $result = $query->setSelect(array('*'))->where('ID', '=', $bookId)->exec();

        $book = $result->fetch();
        return ($book !== false) ? Book::fromArray($book) : null;
    }

    public function findByAuthorId(int $authorId, ?int $limit = null, ?int $offset = null): array {
        $query = $this->queryFactory->createQuery(BookRepository::ENTITY_PERSISTENCE_ID);
        $result = $query->setSelect(array('*'))
            ->setLimit($limit)
            ->setOffset($offset)
            ->where('AUTHOR_ID', '=', $authorId)
            ->exec();

        $bookCollection = array();
        while ($book = $result->fetch()) {
            $bookCollection[] = Book::fromArray($book);
        }

        return $bookCollection;
    }


}