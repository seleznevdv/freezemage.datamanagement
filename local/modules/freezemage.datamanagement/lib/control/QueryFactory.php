<?php


namespace freezemage\datamanagement\control;


use Bitrix\Main\ORM\Query\Query;


class QueryFactory {
    public function createQuery(string $persistenceId): Query {
        return new Query($persistenceId);
    }
}