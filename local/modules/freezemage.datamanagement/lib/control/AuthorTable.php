<?php


namespace freezemage\datamanagement\control;


use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\StringField;


class AuthorTable extends DataManager {
    public static function getTableName() {
        return 'fm_author';
    }

    public static function getMap() {
        return array(
            (new IntegerField('ID'))
                ->configurePrimary(true)
                ->configureAutocomplete(true),
            (new StringField('NAME'))
        );
    }
}