<?php


namespace freezemage\datamanagement\entity;


class Book {
    private $id;
    private $name;
    private $authorId;
    private $author;

    public static function fromArray(array $data): Book {
        if (!isset($data['NAME'])) {
            throw new \InvalidArgumentException('Missing book name.');
        }

        return new Book(
            $data['ID'] ?? null,
            $data['NAME'],
            $data['AUTHOR_ID'] ?? null
        );
    }

    public function __construct(
        ?int $id,
        string $name,
        ?int $authorId = null,
        ?Author $author = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->authorId = $authorId;
        $this->author = $author;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function withId(?int $id): Book {
        $book = clone $this;
        $book->id = $id;

        return $book;
    }

    public function getName(): string {
        return $this->name;
    }

    public function withName(string $name): Book {
        $book = clone $this;
        $book->name = $name;

        return $book;
    }

    public function getAuthorId(): ?int {
        return $this->authorId;
    }

    public function withAuthorId(?int $authorId): Book {
        $book = clone $this;
        $book->authorId = $authorId;

        return $book;
    }

    public function getAuthor(): ?Author {
        return $this->author;
    }

    public function withAuthor(?Author $author): Book {
        $book = clone $this;
        $book->author = $author;

        return $book;
    }

    public function toArray(): array {
        return array(
            'NAME' => $this->getName(),
            'AUTHOR_ID' => $this->getAuthorId()
        );
    }
}