<?php


namespace freezemage\datamanagement\entity;


class Author {
    private $id;
    private $name;

    public static function fromArray(array $data): Author {
        return new Author(
            $data['ID'] ?? null,
            $data['NAME']
        );
    }

    public function __construct(?int $id, string $name) {
        $this->id = $id;
        $this->name = $name;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function withId(?int $id): Author {
        $author = clone $this;
        $author->id = $id;

        return $author;
    }

    public function getName(): string {
        return $this->name;
    }

    public function withName(string $name): Author {
        $author = clone $this;
        $author->name = $name;

        return $author;
    }

    public function toArray(): array {
        return array(
            'ID' => $this->getId(),
            'NAME' => $this->getName()
        );
    }
}