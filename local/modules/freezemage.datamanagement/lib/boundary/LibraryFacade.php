<?php


namespace freezemage\datamanager\boundary;


use freezemage\datamanagement\control\BookRepository;
use freezemage\datamanagement\entity\Author;
use freezemage\datamanagement\entity\Book;


class LibraryFacade {
    private $bookRepository;

    public function __construct(BookRepository $bookRepository) {
        $this->bookRepository = $bookRepository;
    }

    public function getAuthor(int $authorId): Author {

    }

    public function getBook(int $bookId): Book {
        $book = $this->bookRepository->findById($bookId);
        if ($book === null) {
            throw new LibraryFacadeException('Book not found.');
        }
    }
}