<?php


use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\ORM\Entity;
use freezemage\datamanagement\control\AuthorTable;
use freezemage\datamanagement\control\BookTable;


class freezemage_datamanagement extends CModule {
    var $MODULE_ID = 'freezemage.datamanagement';
    var $MODULE_NAME = 'Freezemage: data management examples and experiments.';
    var $MODULE_VERSION = '1.0.0';
    var $MODULE_VERSION_DATE = '2020-12-10 23:59:00';

    var $PARTNER_NAME = 'Freezemage0';
    var $PARTNER_URI = 'https://intervolga.ru';

    function DoInstall() {
        try {
            ModuleManager::registerModule($this->MODULE_ID);
            Loader::includeModule($this->MODULE_ID);

            $this->InstallDB();
        } catch (Exception $exception) {
            $this->DoUninstall();
        }
    }

    function InstallDB() {
        $connection = Application::getConnection();
        $entities = array(AuthorTable::getEntity(), BookTable::getEntity());

        foreach ($entities as $entity) {
            /** @var Entity $entity */
            if (!$connection->isTableExists($entity->getDBTableName())) {
                $entity->createDbTable();
            }
        }
    }

    function UnInstallDB() {
        $connection = Application::getConnection();
        $entities = array(AuthorTable::getEntity(), BookTable::getEntity());

        foreach ($entities as $entity) {
            /** @var Entity $entity */
            if ($connection->isTableExists($entity->getDBTableName())) {
                $connection->dropTable($entity->getDBTableName());
            }
        }
    }

    function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);

        $this->UnInstallDB();

        ModuleManager::unRegisterModule($this->MODULE_ID);
    }
}